package ge.edu.btu.homework5

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query

@Dao
interface RecordDao {

    @Query("SELECT * FROM records")
    fun getRecords() : List<Record>

    @Insert
    fun saveRecord(record: Record)

    @Query("DELETE FROM records")
    fun resetRecords()
}