package ge.edu.btu.homework5

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import java.text.SimpleDateFormat
import java.util.Date

class MainActivity : AppCompatActivity() {
    lateinit var dao: RecordDao
    lateinit var runningView: EditText
    lateinit var swimmingView: EditText
    lateinit var calorieView: EditText
    lateinit var submitButton: Button
    lateinit var avgRunningView: TextView
    lateinit var avgSwimmingView: TextView
    lateinit var avgCalorieView: TextView
    lateinit var totalDistanceView: TextView
    lateinit var latestRecordView: TextView
    lateinit var resetButton: Button

    lateinit var record_list: List<Record>
    var tot_run_dist: Float? = null
    var tot_swim_dist: Float? = null
    var tot_cal_int: Int? = null
    var date: Long? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        dao = App.instance.db.getRecordDao()

        runningView = findViewById(R.id.runningDistance)
        swimmingView = findViewById(R.id.swimmingDistance)
        calorieView = findViewById(R.id.caloriesInput)
        submitButton = findViewById(R.id.submitButton)
        avgRunningView = findViewById(R.id.averageRunning)
        avgSwimmingView = findViewById(R.id.averageSwimming)
        avgCalorieView = findViewById(R.id.averageCalorie)
        totalDistanceView = findViewById(R.id.totalDistance)
        latestRecordView = findViewById(R.id.latestRecord)
        resetButton = findViewById(R.id.resetButton)

        calculateStatistics()

        submitButton.setOnClickListener{
            saveRecord()
        }

        resetButton.setOnClickListener {
            wipeRecords()
        }
    }

    fun saveRecord(){
        if(runningView.text.isNotEmpty() && swimmingView.text.isNotEmpty() && calorieView.text.isNotEmpty()){
            date = Date().time

            dao.saveRecord(Record(0,
                runningView.text.toString().toFloat(),
                swimmingView.text.toString().toFloat(),
                calorieView.text.toString().toInt(),
                date
            ))

            calculateStatistics()
        }
    }

    fun wipeRecords(){
        dao.resetRecords()

        avgRunningView.setText("")
        avgSwimmingView.setText("")
        avgCalorieView.setText("")
        totalDistanceView.setText("")
        latestRecordView.setText("")
    }

    fun calculateStatistics(){
        val size: Int
        val avg_run: Float
        val avg_swim: Float
        val avg_cal: Int
        val tot_dist: Float
        val date: String

        record_list = dao.getRecords()
        size = record_list.size

        if(record_list.isNotEmpty()){
            tot_run_dist = 0.0f
            tot_swim_dist = 0.0f
            tot_cal_int = 0

            record_list.forEach {
                    record -> run{
                tot_run_dist = tot_run_dist?.plus(record.run_dist!!)
                tot_swim_dist = tot_swim_dist?.plus(record.swim_dist!!)
                tot_cal_int = tot_cal_int?.plus(record.calorie!!)
            }
            }

            avg_run = tot_run_dist!! / size
            avg_swim = tot_swim_dist!! / size
            avg_cal = tot_cal_int!! / size
            tot_dist = tot_run_dist!! + tot_swim_dist!!
            date = SimpleDateFormat("dd.MM.yyyy HH:mm:ss").format(record_list.last().date)

            avgRunningView.setText("Average Running Distance: $avg_run km")
            avgSwimmingView.setText("Average Swimming Distance: $avg_swim km")
            avgCalorieView.setText("Average Calorie Intake: $avg_cal")
            totalDistanceView.setText("Total Distance: $tot_dist km")
            latestRecordView.setText("Latest Record: $date")
        }
    }
}