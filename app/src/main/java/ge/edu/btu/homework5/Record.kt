package ge.edu.btu.homework5

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "records")
data class Record (
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    val id: Int,

    @ColumnInfo(name = "running_distance")
    val run_dist: Float?,

    @ColumnInfo(name = "swimming_distance")
    val swim_dist: Float?,

    @ColumnInfo(name = "calorie_intake")
    val calorie: Int?,

    @ColumnInfo(name = "record_date")
    val date: Long?
    )