package ge.edu.btu.homework5

import android.app.Application
import androidx.room.Room

class App: Application() {
    lateinit var db: Database

    companion object{
        lateinit var instance: App
        private set
    }

    override fun onCreate() {
        super.onCreate()

        instance = this

        db = Room.databaseBuilder(
            applicationContext,
            Database::class.java,
            DbConfig.DB_NAME,
        ).allowMainThreadQueries().build()
    }
}