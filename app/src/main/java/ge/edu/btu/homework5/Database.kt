package ge.edu.btu.homework5

import androidx.room.Database
import androidx.room.RoomDatabase

@Database(entities = [Record::class], version = DbConfig.VERSION)
abstract class Database: RoomDatabase() {
    abstract fun getRecordDao(): RecordDao
}